'use strict';

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../actions'
import Home from './Home'
import { Linking, AppState, Platform } from 'react-native'
import { getParameterByName } from '../utils'

class AppContainer extends Component {

	_handleOpenURL(event) {
		let code = getParameterByName('code', event.url)
		if(code) {
			this.props.fetchUserAutorization(code)
		}
	}

	componentDidMount() {
		Linking.addEventListener('url', this._handleOpenURL);
	}

	componentWillUnmount () { Linking.removeEventListener('url', this._handleOpenURL) }

	constructor(props) {
		super(props)
		this._handleOpenURL = this._handleOpenURL.bind(this)
	}

	render() {
		return (
			<Home {...this.props} />
		);
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(ActionCreators, dispatch)
}

export default connect((state) => { return {} }, mapDispatchToProps)(AppContainer)