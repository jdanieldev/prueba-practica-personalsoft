'use strict';

import React, { Component } from 'react'

import {
	StyleSheet,
	View,
	Text,
	ScrollView,
	TextInput,
	Image,
	TouchableOpacity,
	Linking,
} from 'react-native'
import { connect } from 'react-redux'
import { styles } from '../styles'
import dismissKeyboard from 'dismissKeyboard'

class Home extends Component {

	constructor(props) {
	  super(props);
	
	  this.state = { searchingFlag: false, imagesInput: '' };
	}

	images() {
		return Object.keys(this.props.searchedImages).map(key => this.props.searchedImages[key]).map(image => {
			return <ImagesItem key={image.id} image={image} />
		})
	}

	searching() {
		return <Text style={{color: '#FFF'}}>Buscando...</Text>
	}

	searchPressed() {
		this.setState({ searchingFlag: true })
		this.props.fetchImages(this.state.imagesInput).then(() => {
			this.setState({ searchingFlag: false })
		})
		dismissKeyboard()
	}

	uploadPressed() {
		if(!this.props.userLogged.token){
			let url = `https://unsplash.com/oauth/authorize
				?response_type=code
				&client_id=11d7ab2a806534f4bfbddf9774860e36ed25f71b3b6d59ed39ba39cb06b71850
				&redirect_uri=photosunsplash://callback
				&scope=public+read_photos+write_photos`
	
			Linking.openURL(url)
		}
	}

	openUrl() {
		
	}

	render() {
		return (
			<View style={styles.scene}>
				<Text style={styles.title}>Fotos de Unsplash</Text>
				<Text style={styles.subtitle}>Por Daniel Hernández @jdanieldev</Text>
				<ScrollView style={styles.scrollSection}>
					{this.state.searchingFlag ? this.searching() : this.images()}
				</ScrollView>
				<View style={styles.searchSection}>
					<TextInput style={styles.searchInput}
						returnKeyType='search'
						placeholder='Buscar fotos...'
						onChangeText={(imagesInput) => { this.setState({imagesInput}) }}
						value={this.state.imagesInput}
						onSubmitEditing={(event) => this.searchPressed()}
						selectTextOnFocus={true}
					/>
					<TouchableOpacity onPress={() => this.searchPressed()} style={styles.searchButton}>
						<Image source={require('../assets/search-icon.png')} />
					</TouchableOpacity>
				</View>
				<View style={styles.uploadButtonBackground} />
				<TouchableOpacity onPress={() => this.uploadPressed()} style={styles.uploadButton}>
					<Image source={require('../assets/upload-icon.png')} />
				</TouchableOpacity>
			</View>
		);
	}
}

function ImagesItem(props) {
	return (
		<View>
			<TouchableOpacity onPress={() => { console.log(props.image.id) }}>
			  <Image source={{uri: props.image.urls.thumb}} style={styles.resultImage} />
			</TouchableOpacity>
			<Text style={styles.attribution}>{props.image.user.name}</Text>
		</View>
	)
}

function mapStateToProps(state) {
	return  {
		searchedImages: state.searchedImages,
		userLogged: state.userLogged
	}
}

export default connect(mapStateToProps)(Home)