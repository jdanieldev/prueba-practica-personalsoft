import * as imagesActions from './images'

export const ActionCreators = Object.assign({},
	imagesActions
)