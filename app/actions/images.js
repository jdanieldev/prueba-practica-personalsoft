import * as types from './types'
import Api from '../lib/api'

export function fetchImages(query) {
	return (dispatch, getState) => {
		const params = [
			`query=${encodeURIComponent(query)}`
		].join('&')

		return Api.get(`/search/photos?${params}`).then(response => {
			dispatch(setSearchedImages({ images: response }))
		}).catch(e => {
			console.log(e)
		}) 
	}
}

export function fetchUserAutorization(code) {
	return (dispatch, getState) => {
		const params = {
			'code': code,
			'client_id': '11d7ab2a806534f4bfbddf9774860e36ed25f71b3b6d59ed39ba39cb06b71850',
			'client_secret': '90fa88da50f80025734ce91363360d1bfc5bfefd62b74c62d8e1c48d35484852',
			'redirect_uri': 'photosunsplash://callback',
			'grant_type': 'authorization_code'
		}

		return Api.post('/oauth/token', params, 2).then(response => {
			dispatch(setUserToken({ token: response }))
		}).catch(e => {
			console.log(e)
		}) 
	}
}

export function setSearchedImages({ images }) {
	return {
		type: types.SET_SEARCHED_IMAGES,
		images
	}
}

export function setUserToken(token) {
	return {
		type: types.SET_LOGGED_USER,
		token
	}
}