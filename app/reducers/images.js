import createReducer from '../lib/createReducer'
import * as types from '../actions/types'


export const searchedImages = createReducer({}, {
	[types.SET_SEARCHED_IMAGES](state, action) {
		let newState = {};
		action.images.results.forEach( image => {
			newState[image.id] = image
		})

		return newState
	}
})

export const userLogged = createReducer({}, {
	[types.SET_LOGGED_USER](state, action) {
		return action.token
	}
})