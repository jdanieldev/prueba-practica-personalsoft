import { combineReducers } from 'redux'
import * as imagesReducer from './images'

export default combineReducers(Object.assign(
	imagesReducer
))