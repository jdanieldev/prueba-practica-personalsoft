import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	scene: {
		flex: 1,
	},
	title: {
		height: 25,
		textAlign: 'center',
		fontSize: 20,
		color: '#000',
		fontWeight: 'bold'
	},
	subtitle: {
		height: 15,
		textAlign: 'center',
		fontSize: 10,
		color: '#000',
	},
	searchSection: {
		height: 50,
		borderTopColor: '#000',
		borderTopWidth: 1,
		padding: 5,
		flexDirection: 'row',
	},
	searchInput: {
		flex: 0.8,
	},
	searchButton: {
		flex: 0.2,
		alignItems: 'center',
		justifyContent: 'center',
		width: 36,
		height: 36,
		borderRadius: 18,
		backgroundColor: '#000',
	},
	uploadButton: {
		width: 50,
		height: 50,
		borderRadius: 25,
		backgroundColor: '#000',
		position: 'absolute',
		bottom: 60,
		right: 10,
		alignItems: 'center',
		justifyContent: 'center',
	},
	uploadButtonBackground: {
		width: 54,
		height: 54,
		borderRadius: 27,
		backgroundColor: '#FFF',
		position: 'absolute',
		bottom: 58,
		right: 8,
	},
	scrollSection: {
		flex: 0.8,
		backgroundColor: '#000'
	},
	resultImage: {
		height: 100
	},
	attribution: {
		color: '#FFF',
		height: 20,
		textAlign: 'center',
		marginBottom: 6
	},
})