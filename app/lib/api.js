class Api {

  static headers() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'dataType': 'json',
      'Authorization': 'Client-ID 11d7ab2a806534f4bfbddf9774860e36ed25f71b3b6d59ed39ba39cb06b71850'
    }
  }

  static get(route) {
    return this.xhr(route, null, 'GET');
  }

  static put(route, params) {
    return this.xhr(route, params, 'PUT')
  }

  static post(route, params, selectHost?) {
    return this.xhr(route, params, 'POST', selectHost)
  }

  static delete(route, params) {
    return this.xhr(route, params, 'DELETE')
  }

  static xhr(route, params, verb, selectHost=1) {
    const host1 = 'https://api.unsplash.com'
    const host2 = 'https://unsplash.com'
    let url

    switch(selectHost) {
      case 1:
        url = `${host1}${route}`
        break
      case 2:
        url = `${host2}${route}`
        break
    }
    
    
    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
    options.headers = this.headers()
    if(params && params.token)
      options.headers.push({'Authorization': `Bearer ${params.token}`})

    return fetch(url, options).then( response => {
      json = response.json();

      if (response.ok && response.status == 200) {
        return json
      }
      return json.then(err => {throw err});
    }).then( json => json );
  }

}

export default Api